.. AUSU documentation master file, created by
   sphinx-quickstart on Sun May 31 00:12:36 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to AUSU's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   tuxera_performance


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
